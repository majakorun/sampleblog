blog.config(['$routeProvider', function($routeProvider){
	$routeProvider.
		when('/', {
			templateUrl: 'app/views/home.html',
			controller: 'HomeController'
		}).
		when('/users/:id?', {
			templateUrl: 'app/views/users.html',
			controller: 'UsersController'
		}).
		when('/posts/:id?', { // /posts/:id
			templateUrl: 'app/views/posts.html',
			controller: 'PostsController'
		}).
				when('/404', {
			templateUrl: 'app/views/404.html'
		}).
		otherwise({
			redirectTo: '/404'
		});
}]);