// DevComment: Always add dependencies for services, controllers, filters, directives etc. in array like brackets
// Example: blog.service('postsService', ['$http', function ($http) { // code }])

blog.controller("PostsController", ['$scope', 'postsService', 'usersService', 'commentService', '$routeParams', function ($scope, postsService, usersService, commentService, $routeParams) {

	if ($routeParams.id) {

		postsService.getPost($routeParams.id).success(function (response) {
			$scope.posts = [response];
			$scope.singlePost = true;
			// 1. OPCIJA
			// usersService.getUser(response.userId).success(function (userResponse) {
			// 	$scope.user = userResponse;
			// });
		});
		
		commentService.getComment($routeParams.id).success(function (response) {
			$scope.comments = response;	
		});

	} else {
		postsService.getPosts().success(function (response) {
			$scope.posts = response;
		});
	}
}]);