// DevComment: Always add dependencies for services, controllers, filters, directives etc. in array like brackets
// Example: blog.service('postsService', ['$http', function ($http) { // code }])

blog.controller("UsersController", function ($scope, usersService, $resource, $routeParams)
{
	if ($routeParams.id) {
		usersService.getUser($routeParams.id).success(function (response) {
			$scope.users = [response];
			$scope.singlePost = true;
		});
		
	} else {
		usersService.getUsers().success(function (response) {
			$scope.users = response;
		});
	}

})