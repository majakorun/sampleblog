blog.directive('comment', function (commentService) {
	'use strict';

	return {
		replace: true,
		templateUrl: '/app/directives/comment/comment.html',
		scope: {
			name: '@',
			body: '@',
			postId: '@',
			singlePost: '@',
			email: '@'
		},
		link: function (scope, element, attrs) {
		}
	};
});