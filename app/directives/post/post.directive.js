// DevComment: Always add dependencies for services, controllers, filters, directives etc. in array like brackets
// Example: blog.service('postsService', ['$http', function ($http) { // code }])

blog.directive('post', function (usersService, commentService) {
	'use strict';

	return {
		replace: true,
		templateUrl: '/app/directives/post/post.html',
		scope: {
			title: '@',
			body: '@',
			postId: '@',
			singlePost: '@',
			userId: '@'
		},
		link: function (scope, element, attrs) {

			// DevComment: You can use single condition here. Both `if` are validating same condition below.
			if (scope.singlePost) {
				usersService.getUser(scope.userId).success(function (userResponse) {
					scope.userData = userResponse;
				});
			}

			if (scope.singlePost) {
				commentService.getComment(scope.postId).success(function (commentResponse) {
					scope.commentData = commentResponse;
				});
			}
		}
	};
});