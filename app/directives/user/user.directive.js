blog.directive('user', function () {
	'use strict';

	return {
		replace: true,
		templateUrl: '/app/directives/user/user.html',
		scope: {
			name: '@',
			username: '@',
			singlePost: '@',
			userId: '@',
			address: '=',
			company: '=',
			website: '=',
			users: '=',
			index: '@'
		},
		link: function (scope, element, attrs) {

			scope.delete = function (index) { 
				scope.users.splice(index, 1);
			};
		}
	};
});