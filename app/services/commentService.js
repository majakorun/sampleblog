blog.service("commentService", ['$http', function ($http) {
	var _this = {};

	_this.getComment = function (postId) {
	
		return $http.get('http://jsonplaceholder.typicode.com/posts/' + postId + '/comments');
	};

	return _this;
}]);