blog.service("postsService", ['$http', function ($http) {
	var _this = {};

	_this.getPost = function (postId) {
		if (!postId) {
			return;
		}

		return $http.get('http://jsonplaceholder.typicode.com/posts/' + postId);
	};

	_this.getPosts = function () {
		return $http.get('http://jsonplaceholder.typicode.com/posts');
	};

	return _this;
}]);
