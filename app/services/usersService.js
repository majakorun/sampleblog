blog.service("usersService", ['$http', function ($http) {
	var _this = {};

	_this.getUser = function (userId) {
		if (!userId) {
			return;
		}

		return $http.get('http://jsonplaceholder.typicode.com/users/' + userId);
	};

	_this.getUsers = function () {
		return $http.get('http://jsonplaceholder.typicode.com/users');
	};

	return _this;
}]);
